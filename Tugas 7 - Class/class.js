console.log('----------- Soal No 1------------')
class Animal {
    constructor(name, legs, cold_blooded) {
        this.name = name
        this.legs = legs
        this.cold_blooded = cold_blooded
    }
}
 
sheep = new Animal("shaun", 4 , "false");
console.log('Release 0')
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log(sheep)
console.log('-----------------------------------')

class Ape extends Animal {
  constructor(name, legs, cold_blooded) {
    super(name, cold_blooded);
    this.legs = legs;
  }
   static yell() {
    return "Auoo uoo!";
  }
}
class Frog extends Animal {
  constructor(name, legs, cold_blooded) {
    super(name, cold_blooded);
    this.legs = legs;
    }
    static jump() {
    return "Hoop hoop!!";
  }
}
console.log('Release 1')
sungokong = new Animal("kera sakti", 2 , "true", "Auooo");
console.log(Ape.yell());
console.log(sungokong)

kodok = new Animal("buduk", 2 , "true");
console.log(Frog.jump());
console.log(kodok)

console.log('-------- Soal No 2------------')
class Clock { 
  constructor ({template }) {
    this.template = template;
    }
  
  render(){

    this.date = new Date();

    this.hours = this.date.getHours();
    if (this.hours < 10) hours = '0' + this.hours;
    
    this.minutes = this.date.getMinutes();
    if (this.minutes < 10) minutess = '0' + this.minutes;    
    
    this.seconds = this.date.getSeconds();
    if (this.seconds < 10) seconds = '0' + this.seconds
    
    this.output = this.template.replace('h', this.hours).replace('m',this.minutes).replace('s',this.seconds);
  
    console.log(this.output);
  }
  stop(){
    clearInterval(this.timer);
  }
   start(){
    this.render();
    this.timer = setInterval(this.render.bind(this), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 