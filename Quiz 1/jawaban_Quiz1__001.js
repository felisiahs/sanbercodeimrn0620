console.log('------------ SOAL  A-------');
function balikKata (str) {
  let result = '';
  for (let i = str.length-1;
  i>=0; i--) {
    result += str[i];
  }
  return result;
};
console.log(balikKata("abcde")) // edcba
console.log(balikKata("rusak")) // kasur
console.log(balikKata("racecar")) // racecar
console.log(balikKata("haji")) // ijah
console.log()

console.log('------------ SOAL B-------');

function palindrome (str) {
  let result = '';
  for (let i = str.length-1;
  i>=0; i--) {
    result += str[i];
  }
  if (result===str) {
  return true;
  } else{
    return false
  }
  return result;
};

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false
console.log()

console.log('------------ SOAL C-------');
function bandingkan(num1, num2) {
  if (num1 < 0){
    return -1;
  } else if (num1===num2) {
    return -1;
    } else if (num1>num2) {
      return num1;
    } else if (num1<num2) {
      return num2;
    } else  {
    return num1;
    }
}


console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18
