console.log('------------ SOAL  A-------');
function balikKata (str) {
  let result = '';
  for (let i = str.length-1;
  i>=0; i--) {
    result += str[i];
  }
  return result;
};
console.log(balikKata("abcde")) // edcba
console.log(balikKata("rusak")) // kasur
console.log(balikKata("racecar")) // racecar
console.log(balikKata("haji")) // ijah
console.log()

console.log('------------ SOAL B-------');

function palindrome (str) {
  let result = '';
  for (let i = str.length-1;
  i>=0; i--) {
    result += str[i];
  }
  if (result===str) {
  return true;
  } else{
    return false
  }
  return result;
};

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false
console.log()

console.log('------------ SOAL C-------');
function bandingkan(num1, num2) {
  if (num1 < 0){
    return -1;
  } else if (num1===num2) {
    return -1;
    } else if (num1>num2) {
      return num1;
    } else if (num1<num2) {
      return num2;
    } else  {
    return num1;
    }
}


console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18
console.log()
console.log()
console.log()

console.log('------------ SOAL  002.js-------');
console.log('------------ SOAL  A-------');
function AscendingTen(num=0) {
  // Tulis code kamu di sini
  var deret = ''
  if (num>1) {
    for(i=num;i<num+10;i++){
        deret = deret + i + ' '
    }
  }else{
      return -1
  }
  return deret
}
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1
console.log()
console.log('------------ SOAL  B-------');

function DescendingTen(num=0) {
  // Tulis code kamu di sini
  var deret = ''
  if (num>1) {
    for(i=num;i>num-10;i--){
        deret = deret + i + ' '
    }
  }else{
      return -1
  }
  return deret
}

console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

// TEST CASES Descending Ten
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1
console.log()

console.log('------------ SOAL  C-------');

function ConditionalAscDesc(reference, check) {
  // Tulis code kamu di sini
  var deret = ''
  if (reference>1 && check>0) {
    if (check%2!=0) {
        for(i=reference;i<reference+10;i++){
            deret = deret + i + ' '
        }
    }else{
      for(i=reference;i>reference-10;i--){
        deret = deret + i + ' '
        }   
    }
  }else{
      return -1
  }
  return deret
}
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1
console.log()

console.log('------------ SOAL  D-------');
function ularTangga() {
  // Tulis code kamu di sini
  j=0
    deret=''
    for(i=10;i>0;i--){
        if (i%2 !=0) {
            j=i*10-9
            while(j<(i*10+1)) {
                deret = deret + j + ' '
                j++
            }
        }else{
            j=i*10
            while(j>=(i*10-9)) {
                deret = deret + j + ' '
                j--
            }
        }
        deret +='\n'
    }
    return deret
}
console.log(ularTangga()) 

