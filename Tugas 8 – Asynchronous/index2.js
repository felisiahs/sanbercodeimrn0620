var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let myTime = 10000;
 function timeCount(i){
   if ( i == books.length){
     return 0
   } else {
     readBooks(myTime,books[i], function(check){
       myTime -= books[i].timeSpent;
       timeCount(i+1);
     }
     );
   }
 }
 timeCount(0);