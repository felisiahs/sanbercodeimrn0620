

console.log('----------Tugas Looping No 1----------');
console.log('LOOPING PERTAMA');

var i = 2;
while(i < 21) {
  console.log(i+ ' - I Love Coding' );
  i+=2; 
};

console.log('LOOPING KEDUA');

var i = 20;
while(i > 0) {
  console.log(i+ ' - I Will Become a Mobile Developer' );
  i-=2; 
};



console.log('----------Tugas Looping No 2----------');

var i = 1
while ( i < 20){
  console.log(i+ ' Santai');
  i++;
  console.log(i+ ' Berkualitas');
  i++;
  console.log(i+ ' I Love Coding');
  i++;
  console.log(i+ ' Berkualitas');
  i++;
  console.log(i+ ' Santai');
  i++;
  console.log(i+ ' Berkualitas');
  i++;
  console.log(i+ ' Santai');
  i++;
  console.log(i+ ' Berkualitas');
  i++;
  console.log(i+ ' I Love Coding');
  i++;
  console.log(i+ ' Berkualitas');
  i++;
  console.log(i+ ' Santai');
  i++;
  console.log(i+ ' Berkualitas');
  i++;
  console.log(i+ ' Santai'); 
  i++;
  console.log(i+ ' Berkualitas');
  i++;
  console.log(i+ ' I Love Coding');
  i++;
  console.log(i+ ' Berkualitas');
  i++;
  console.log(i+ ' Santai');
  i++;
  console.log(i+ ' Berkualitas');
  i++;
  console.log(i+ ' Santai');
  i++;
  console.log(i+ ' Berkualitas');
}
  
console.log('----- SOAL MENANTANG NO 2-----')

var nilai = prompt('Masukkan nilai')
    if(nilai % 2 == 0  ) {
        console.log(nilai + ' - Berkualitas ')    
    } else if( nilai % 3 ==  0 && nilai % 2 == 1) {
        console.log(nilai + ' - I Love Coding ')
} else {
    console.log(nilai + ' - Santai ')
}

console.log('----------Tugas Looping No 3----------');
var j = 0;
while (j < 4){ console.log('#########')
j++
}

console.log('----------Tugas Looping No 4----------');

for(var i=1; i<=7; i++){
   console.log("#".repeat(i));
}


console.log('----------Tugas Looping No 5----------');

var i = 8; 
var j = "";
for (var y = 0; y < i; y++) {
  for (var x = 0; x < i; x++) {
    if ((x + y) % 2 == 0)
      j += " ";
    else
      j += "#";
  }
  j += "\n";
}

console.log(j);