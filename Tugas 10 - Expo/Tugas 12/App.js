import React, { Component } from 'react'
import{
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/videoItem'
import data from './data.json'
import Component from './Tugas12/Component/Component';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar} >
        <Image source= {require('./images/logo.png')} style = {{width :98, height:22}}/>
        <View style={styles.rightNav}>
            <TouchableOpacity>
            <icon style={style.navItem} name="search" size={25}/>
            </TouchableOpacity>
            <TouchableOpacity>
            <icon style={style.navItem} name="account-circle" size={25}/>
            </TouchableOpacity>
            </View>
            </View>
        <View style={styles.body} />
        <VideoItem video={data.item{0}/>
        <FlatList
        data={data.items}
        renderItems={(video)=><VideoItem video={video.item} />}
        keyExtractor={(item)=>item.id)}
        itemSeparateComponent={()=><View style={{height: 0.5,backgroundColor:'#e5e5e5'}}/>}
        />
        </View>
        <View style={styles.tabBar}>
            <TouchableOpacity style={styles.tabItem}>
                <Icon name="home" size={25}/>
                <Text style={styles.tabTitle}> Home </Text>
            </TouchableOpacity>
            <TouchableOpacity styles={styles.tabItem}>
                <Icon name="whatshot" size={25}/>
                <Text style={styles.tabTitle}> Trending </Text>
            </TouchableOpacity>
            <TouchableOpacity styles={styles.tabItem}>
                <Icon name="subcription" size={25}/>
                <Text style={styles.tabTitle}> Subcription </Text>
            </TouchableOpacity>            
            <TouchableOpacity styles={styles.tabItem}>
                <Icon name="folder" size={25}/>
                <Text style={styles.tabTitle}> Library </Text>
            </TouchableOpacity>
        </View>
      </View>
      </View>
    );
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  rightNav : {
      flexDirection : 'row'
  },
  rightNav: {
    flexDirection: 'row'
  },
  navItem: {
    marginLeft: 25
  },
  body: {
    flex: 1
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTitle: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 4
  }
});